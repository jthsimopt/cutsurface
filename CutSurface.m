classdef CutSurface < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        P
        T
        surfh
        nTri
        nnod
        CutEle
    end
    
    properties(Access = private)
        surfaceArea_
    end
%     
%     properties (Dependent)
%         surfaceArea
%     end
        
    
    methods
        % Constructor
        function O = CutSurface(P,T,surfh)
            % Surface(P,T)
            O.P = P;
            O.T = T;
            O.nTri = size(T,1);
            O.nnod = size(P,1);
            O.surfh = surfh;
            O.surfaceArea_ = [];
            O.CutEle = unique([surfh.iel]).';
        end
        
        function area = surfaceArea(O)
            if isempty(O.surfaceArea_)
                % Compute the surfaceArea
                O.surfaceArea_ = 0;
                for iTri = 1:length(O.surfh)
                    O.surfaceArea_ = O.surfaceArea_ + O.surfh(iTri).Area;
                end
                area = O.surfaceArea_;
            else
               area = O.surfaceArea_;
           end
        end
        
        
        function h = Visualize(O,varargin)
            % h = Visualize()
            
            %% Check for xfigure
            if exist('xfigure','file') == 2
                h.fig = xfigure;
            else
                RequiredFileMissing('xfigure', 'https://raw.githubusercontent.com/cenmir/xfigure/master/xfigure.m')
                RequiredFileMissing('xfigure_KPF', 'https://raw.githubusercontent.com/cenmir/xfigure/master/xfigure_KPF.m')
                h.fig = xfigure;
            end
            
            %% Draw the patch
            h.patch = patch('Faces',O.T,'Vertices',O.P, 'FaceColor','w');
            axis equal; view(3); h.light(1) = light;
            xlabel('X'); ylabel('Y'); zlabel('Z');
            h.fig.Color = 'w';
            
            
           
        end
        
    end
    
    %Hide some of the inherited methods from handle
    methods(Hidden)
      function lh = addlistener(varargin)
         lh = addlistener@handle(varargin{:});
      end
      function notify(varargin)
         notify@handle(varargin{:});
      end
      function delete(varargin)
         delete@handle(varargin{:});
      end
      function Hmatch = findobj(varargin)
         Hmatch = findobj@handle(varargin{:});
      end
      function p = findprop(varargin)
         p = findprop@handle(varargin{:});
      end
      function TF = eq(varargin)
         TF = eq@handle(varargin{:});
      end
      function TF = ne(varargin)
         TF = ne@handle(varargin{:});
      end
      function TF = lt(varargin)
         TF = lt@handle(varargin{:});
      end
      function TF = le(varargin)
         TF = le@handle(varargin{:});
      end
      function TF = gt(varargin)
         TF = gt@handle(varargin{:});
      end
      function TF = ge(varargin)
         TF = ge@handle(varargin{:});
      end
   end
    
end

function RequiredFileMissing(filename, RemoteDestination)
    %If we're going to download a whole bunch of files, it is better to set
    % RequiredFilesDir to be a global and not have to ask the user to
    % specify a destination folder for every file...
    global RequiredFilesDir
    
    
    disp([filename,' is missing!'])
    disp(['Trying to download ',filename,' from ',RemoteDestination])
    
    
    if isempty(RequiredFilesDir)
        scriptPath = mfilename('class');
        [ScriptDir,~,~] = fileparts(scriptPath);
        DestDir = uigetdir(ScriptDir,['Select where to save ',filename,'. Make sure its either the script directory or a directory on the Path.']);
        if DestDir == 0
            error(['Failed to select folder, failed to install ',filename])
        end
        
        RequiredFilesDir = DestDir;
    end
    DestFile= [RequiredFilesDir,'/',filename];
    
    % Download the RemoteFile and save it to DestFile
    websave(DestFile,RemoteDestination);
    
    % Give up to 10 seconds for the file to show up, otherwise send error
    % message.
    tic
    while 1
        if exist('xfigure','file') == 2
            break
        end
        pause(0.1)
        t1 = toc;
        if t1 > 10
            error(['Failed to download ',filename,'! Timeout.'])
        end
    end

    
end