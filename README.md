# CutSurface
CutSurface class.

`O = CutSurface(P,T,surfh)`

## Properties
Properties    | Description
------------- | -------------
*P*           | Points
*T*           | Topology/ Connectivity
*surfh*       | Array of structs. Contains *nTri* elements
*nTri*        | Number of triangle elements
*nnod*	      | Number of nodes
*CutEle*      | List of all parent elements that are cut

## Methods
### Visualize()
    h = Visualize()
Plots the surface in a figure window using [xfigure](https://bitbucket.org/jthsimopt/xfigure "https://bitbucket.org/jthsimopt/xfigure").

*h* is a handle struct containing

- h.fig - The figure handle
- h.patch - The patch handle

### surfaceArea()
	area = surfaceArea()

Computes the surface area once and stores it in a private variable. For every other call to this function it returns the variable instead of re-computing the area.